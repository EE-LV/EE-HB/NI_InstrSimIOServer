﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="11008008">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This project is used to maintain the IO-Server-VI for a custom IO-Server for the NI Instrument Simulator.

Lizenziert unter EUPL V. 1.1

Copyright 2012 GSI Helmholtzzentrum für Schwerionenforschung GmbH

Dr. Holger Brand, H.Brand@gsi.de
Planckstr. 1, 64291 Darmstadt, Germany</Property>
	<Property Name="varPersistentID:{03A4F379-B383-4764-8DA0-15552876149C}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Error Message</Property>
	<Property Name="varPersistentID:{05F48DEF-FE38-4DCF-BE53-CB5A9C2E4EFA}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Self-Test Result Message</Property>
	<Property Name="varPersistentID:{0676313D-E2BA-4B09-B986-CD7EBF407DD8}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/DC Voltage Range (1:10 VDC)</Property>
	<Property Name="varPersistentID:{3039017A-9D13-4362-820A-6BBC77B9DB18}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Instrument Driver Revision</Property>
	<Property Name="varPersistentID:{4AD8C634-2D6F-47DB-B61F-EFBF9C20D01C}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/ID Error Source</Property>
	<Property Name="varPersistentID:{5F66B469-7B88-459D-8206-936874106E2E}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/VISA resource name</Property>
	<Property Name="varPersistentID:{62900507-E929-4D90-B1C4-1285ED3AC308}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Selftest OK</Property>
	<Property Name="varPersistentID:{64B2CE28-0768-42C2-B162-591E93A9F432}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Error Code</Property>
	<Property Name="varPersistentID:{6BA20F6B-156C-4FE2-86DD-7946585847A9}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/DC-Measurement</Property>
	<Property Name="varPersistentID:{9A4856B6-6BD4-4C58-9D05-53A26C72EF9F}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Server Type</Property>
	<Property Name="varPersistentID:{B1867459-B252-4509-B3B0-BCAB6DE1F23B}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Active</Property>
	<Property Name="varPersistentID:{B264BBB7-6567-46DA-9647-8F5ED022DD9A}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Self-Test Result Code</Property>
	<Property Name="varPersistentID:{CB040EBF-AA11-487D-A39B-4A8361E35906}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/ID Error Code</Property>
	<Property Name="varPersistentID:{CB217674-B528-4933-8000-D9489D436C55}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Interval</Property>
	<Property Name="varPersistentID:{DB61C6DE-D440-401B-8F77-3B3C7B800382}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Instrument Firmware Revision</Property>
	<Property Name="varPersistentID:{DDEA55F7-2590-4618-92F8-81135CBDA5FC}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Waveform Function (0:Sine)</Property>
	<Property Name="varPersistentID:{DFBB7A9A-82B6-4BDC-96FB-FF8435C60323}" Type="Ref">/My Computer/myNIINstrSimIOServer.lvlib/Waveform</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
		<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
		<Item Name="NI Instrument Simulator.lvlibp" Type="LVLibp" URL="../PPL/NI Instrument Simulator.lvlibp">
			<Item Name="Public" Type="Folder">
				<Item Name="Configure" Type="Folder">
					<Item Name="Configure Waveform.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure Waveform.vi"/>
					<Item Name="Configure Waveform Format.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure Waveform Format.vi"/>
					<Item Name="Configure DC Voltage.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure DC Voltage.vi"/>
					<Item Name="Configure.mnu" Type="Document" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure.mnu"/>
				</Item>
				<Item Name="Data" Type="Folder">
					<Item Name="Read Waveform.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Data/Read Waveform.vi"/>
					<Item Name="Read DC Measurement.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Data/Read DC Measurement.vi"/>
					<Item Name="Data.mnu" Type="Document" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Data/Data.mnu"/>
				</Item>
				<Item Name="Utility" Type="Folder">
					<Item Name="Reset.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Reset.vi"/>
					<Item Name="Revision Query.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Revision Query.vi"/>
					<Item Name="Self-Test.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Self-Test.vi"/>
					<Item Name="Write.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Write.vi"/>
					<Item Name="Read.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Read.vi"/>
					<Item Name="Utility.mnu" Type="Document" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Utility.mnu"/>
				</Item>
				<Item Name="dir.mnu" Type="Document" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/dir.mnu"/>
				<Item Name="Close.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Close.vi"/>
				<Item Name="Initialize.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Initialize.vi"/>
				<Item Name="VI Tree.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/VI Tree.vi"/>
			</Item>
			<Item Name="Private" Type="Folder">
				<Item Name="Get Waveform Scaling.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Private/Get Waveform Scaling.vi"/>
			</Item>
			<Item Name="NI Instrument Simulator Readme.html" Type="Document" URL="../PPL/NI Instrument Simulator Readme.html"/>
			<Item Name="whitespace.ctl" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			<Item Name="Trim Whitespace.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
			<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../PPL/NI Instrument Simulator.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
		</Item>
		<Item Name="NIInstrSimIOServer.lvlib" Type="Library" URL="../NIInstrSimIOServer/NIInstrSimIOServer.lvlib"/>
		<Item Name="myNIINstrSimIOServer.lvlib" Type="Library" URL="../myNIINstrSimIOServer/myNIINstrSimIOServer.lvlib"/>
		<Item Name="Test.vi" Type="VI" URL="../myNIINstrSimIOServer/Test.vi"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="My Source Distribution" Type="Source Distribution">
				<Property Name="Bld_buildCacheID" Type="Str">{9E7F7BF8-43ED-4692-8E65-D4B16ADFA779}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">My Source Distribution</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">//winfilesva/DVEE$Root/Brand/Eigene Dateien/LV2011 Data/InstCache</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">5</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/My Source Distribution</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{ADD95E18-7D3C-44FF-80B0-3A05B7DB0D23}</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/My Source Distribution</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/My Source Distribution/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{6AC73B16-85B3-4C16-BD31-A40DFA8BDEE5}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref"></Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].preventRename" Type="Bool">true</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/NIInstrSimIOServer.lvlib</Property>
				<Property Name="Source[5].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
			</Item>
		</Item>
	</Item>
</Project>
