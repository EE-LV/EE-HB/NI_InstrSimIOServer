﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Active" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Server Status\Active</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"%!A!A!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Status\Active</Property>
	</Item>
	<Item Name="DC Voltage Range (1:10 VDC)" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\DC Voltage Range (1:10 VDC)</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"%!A!A!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">DC Voltage Range (1:10 VDC)</Property>
	</Item>
	<Item Name="DC-Measurement" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:AckType" Type="Str">User</Property>
		<Property Name="Alarming:HiHi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Area" Type="Str">Test</Property>
		<Property Name="Alarming:HiHi:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:HiHi:Description" Type="Str">HIHI</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Limit" Type="Str">90</Property>
		<Property Name="Alarming:HiHi:Name" Type="Str">HIHI</Property>
		<Property Name="Alarming:HiHi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">Test</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">LOLO</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LOLO</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str">Test</Property>
		<Property Name="Alarming:Status:Description" Type="Str">Bad</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">15</Property>
		<Property Name="featurePacks" Type="Str">Network,Alarming</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\DC-Measurement</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"%!A!A!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">DC-Measurement</Property>
	</Item>
	<Item Name="Error Code" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Server Status\Error Code</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"%!A!A!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Status\Error Code</Property>
	</Item>
	<Item Name="Error Message" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Server Status\Error Message</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"%!A!A!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Status\Error Message</Property>
	</Item>
	<Item Name="ID Error Code" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\ID Error Code</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"%!A!A!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">ID Error Code</Property>
	</Item>
	<Item Name="ID Error Source" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\ID Error Source</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"%!A!A!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">ID Error Source</Property>
	</Item>
	<Item Name="Instrument Driver Revision" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Instrument Driver Revision</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"%!A!A!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Instrument Driver Revision</Property>
	</Item>
	<Item Name="Instrument Firmware Revision" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Instrument Firmware Revision</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"%!A!A!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Instrument Firmware Revision</Property>
	</Item>
	<Item Name="Interval" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Interval</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"%!A!A!!!!"!!5!#A!!!1!!0`!!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Interval</Property>
	</Item>
	<Item Name="NIInstrSim-IO-Server" Type="IO Server">
		<Property Name="atrb" Type="Str">1A)!!!Q1!!!'!!!!"B!!!"%!!!"$!']!&lt;A"G!'E!:Q"V!()!91"U!'E!&lt;Q"O!%Y!91"N!'5!"B!!!"1!!!"/!%E!31"O!(-!&gt;!"S!&amp;-!;1"N!#U!31"0!#U!5Q"F!()!&gt;A"F!()!"B!!!"M!!!"%!%-!)!"7!']!&lt;!"U!'%!:Q"F!#!!5A"B!'Y!:Q"F!#!!+!!R!$I!-1!Q!#!!6A"%!%-!+1!$!!!!!!!!!!!!]$]'%!!!#!!!!%E!&lt;A"U!'5!=A"W!'%!&lt;!!(-!!!$A!!!!-!"!!!!!!!!!!M1!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!1!-!"!!!!!!!!!!11!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!!!-!"!!!!!!!!!!!!!-!"!!!!!!!!!"#1!-!"!!!!!!!!!"11!91!!!0!!!!4!"B!')!6A"*!%5!6Q!A!&amp;9!:1"S!(-!;1"P!'Y!!Q!!!!!!!!!!!#:!"B!!!")!!!"7!%E!5Q""!#!!=A"F!(-!&lt;Q"V!()!9Q"F!#!!&lt;A"B!'U!:1!'%!!!%!!!!%=!5!"*!%)!-!![!$I!-1!T!$I!/A"*!%Y!5Q"5!&amp;)!"B!!!"I!!!"8!'%!&gt;A"F!'9!&lt;Q"S!'U!)!"'!(5!&lt;A"D!(1!;1"P!'Y!)!!I!$!!/A"4!'E!&lt;A"F!#E!!Q!!!!!!!!!!!!!!</Property>
		<Property Name="className" Type="Str">Custom VI - Periodic</Property>
	</Item>
	<Item Name="Self-Test Result Code" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Self-Test Result Code</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"%!A!A!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Self-Test Result Code</Property>
	</Item>
	<Item Name="Self-Test Result Message" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Self-Test Result Message</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"%!A!A!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Self-Test Result Message</Property>
	</Item>
	<Item Name="Selftest OK" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Selftest OK</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"%!A!A!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Selftest OK</Property>
	</Item>
	<Item Name="Server Type" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Server Status\Server Type</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"%!A!A!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Server Status\Server Type</Property>
	</Item>
	<Item Name="VISA resource name" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\VISA resource name</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!A(!!!!"%!A!A!!!!"!!A!-0````]!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">VISA resource name</Property>
	</Item>
	<Item Name="Waveform" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Waveform</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"44Q!!!"%!A!A!!!!"!!9!6!!$!!%!!!!!!!!!!!!!!!!!!!!!!!!`]!!!!!!!!!!!!!!!!!!!!!!!!!!2!)!)!!!!!1!%!!!!!1!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Waveform</Property>
	</Item>
	<Item Name="Waveform Function (0:Sine)" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\Waveform Function (0:Sine)</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NIInstrSim-IO-Server.lvproj/My Computer/myNIINstrSimIOServer.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"%!A!A!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\myNIINstrSimIOServer.lvlib\NIInstrSim-IO-Server\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">Waveform Function (0:Sine)</Property>
	</Item>
</Library>
